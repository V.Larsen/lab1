package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> sumRow = new ArrayList<>();
        ArrayList<Integer> sumCol = new ArrayList<>();

        for (int i = 0 ; i < grid.size() ; i++) {
            int sum = 0;
            for (int j = 0 ; j < (grid.get(i)).size() ; j++) {
                sum = sum + grid.get(i).get(j);
            }
            sumRow.add(sum);
        }
        for (int j = 0 ; j < (grid.get(0)).size() ; j++) {
            int sum = 0;
            for (int i = 0 ; i < grid.size() ; i++) {
                sum = sum + grid.get(i).get(j);
            }
            sumCol.add(sum);
        }
        int rowCheck = sumRow.get(0);
        int colCheck = sumCol.get(0);

        for (int i = 0 ; i < sumRow.size() ; i++) {
            if (sumRow.get(i) != rowCheck) {
                return false;
            }
        }
        for (int i = 0 ; i < sumCol.size() ; i++) {
            if (sumCol.get(i) != colCheck) {
                return false;
            }
        }
        return true;

    }

}