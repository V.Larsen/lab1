package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
        multiplicationTable(2);
        crossSum(12);
    }

    public static void multiplesOfSevenUpTo(int n) {
        int i = 7;
        while (i <= n) {
            System.out.println(i);
            i = i + 7;
        }

    }
   
    public static void multiplicationTable(int n) {
        for (int i = 1 ; i <= n ; i++) {
            System.out.print(i + ": ");
            for (int j = 1 ; j <= n ; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }
   
    public static int crossSum(int num) {
        int tverrsum = 0;
        while (num > 0) {
            tverrsum = tverrsum + (num % 10);
            num = num / 10;
        }
        return (tverrsum);
        
    }

}